package com.oracle.tools.observe.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="FileDetail")
public class FileDetail {
	@Id
    @Column
	String filePath;
	
	@Column
	Integer lineNumber;
	

}
