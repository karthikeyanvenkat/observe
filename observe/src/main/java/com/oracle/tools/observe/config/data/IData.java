package com.oracle.tools.observe.config.data;

import java.util.List;

public interface IData {
	/*
	 * public List<DataPOJO> getData(); public void setData(DataPOJO rowData);
	 */
	public List<DataBean> getData();
	public void setData(List<DataBean> rowData);
}
