package com.oracle.tools.observe;

import static com.oracle.tools.observe.logging.LoggingFileType.APPLICATION_LOGGER;
import static com.oracle.tools.observe.logging.LoggingFileType.ERROR_LOGGER;
import static com.oracle.tools.observe.logging.LoggingFileType.PERFORMANCE_LOGGER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.oracle.tools.observe.config.loader.CsvDataLoader;
import com.oracle.tools.observe.logging.ApplicationLogger;
import com.oracle.tools.observe.processor.ScheduledTaskCreator;
import com.oracle.tools.observe.validator.ConfigExcelValidator;

@SpringBootApplication
@ComponentScan(basePackages = {"com.oracle.tools.observe.*"})
@EnableJpaRepositories("com.oracle.tools.observe.*")
@EntityScan("com.oracle.tools.observe.*")
public class ObserveApplication implements CommandLineRunner {
	@Autowired
	private ConfigExcelValidator configExcelValidator;
	@Autowired
	private CsvDataLoader dataLoader;
	@Autowired
	private ScheduledTaskCreator scheduledTaskCreator;
	
	
	public static void main(String[] args) {
		SpringApplication.run(ObserveApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## Performence logs START ##########");
		validateLoadFile(args);
		startMontoring();
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## Performence logs END ##########");
	}

	private void startMontoring() {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## Performence logs Start ##########");
		scheduledTaskCreator.initilize();
		ApplicationLogger.trace(APPLICATION_LOGGER, "Starting Observation");
		scheduledTaskCreator.startObservation();
		while(true)
		{
			try {
				Thread.sleep(360000000);
			} catch (InterruptedException e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "Mian hread Interrupted" , e);
			}
		}
	}

	private void validateLoadFile(String[] args) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## START ##########");
		if (args.length == 1) {
			checkResult(configExcelValidator.validateFile(args[0]), false);
			checkResult(dataLoader.loadData(args[0]), false);
		} else {
			ApplicationLogger.error(ERROR_LOGGER, "Usage: First parameter should be config Excel full path");
			System.exit(1);
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## END ##########");
	}

	private void checkResult(boolean result, boolean isFinalResult) {
		ApplicationLogger.trace(APPLICATION_LOGGER, "result: " + result + " isFinalResult: " + isFinalResult);
		if (!result)
			System.exit(1);
		else {
			if (isFinalResult)
				System.exit(0);
		}

	}
}
