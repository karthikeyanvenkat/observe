package com.oracle.tools.observe.config.loader;

public interface IDataLoader {
	public boolean loadData(String fileFullPath);
}
