package com.oracle.tools.observe.tests;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {
	public static void main(String args[])
	{
		String s = "c:\\parag\\khambia";
		System.out.println(s);
		System.out.println(s.lastIndexOf(File.separator));
		System.out.println(s.substring(0,s.lastIndexOf(File.separator)));
		System.out.println(File.pathSeparator);
		System.out.println(File.separator);
		String matching = "abc*def.xml";
		matching = matching.replaceAll("\\.", "\\\\.");
		matching = matching.replaceAll("\\*", ".*");
		System.out.println(matching);
		Pattern pattern = Pattern.compile("abc.*def\\.xml");
	    Matcher matcher = pattern.matcher("abcXXXXdef.xml");
	    if(matcher.find())
	    	System.out.println("Found");
	    else
	    	System.out.println("Not Found");
	}
}
