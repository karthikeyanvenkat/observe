package com.oracle.tools.observe.files;

import static com.oracle.tools.observe.logging.LoggingFileType.ERROR_LOGGER;
import static com.oracle.tools.observe.logging.LoggingFileType.APPLICATION_LOGGER;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oracle.tools.observe.db.operations.FileDetailDB;
import com.oracle.tools.observe.logging.ApplicationLogger;

@Component
public class ApplicationFileReader {

	@Autowired
	FileDetailDB dbOperations;

	private Set<String> filterKeywords;

	public List<String> readFile(Path sourceFile, String searchChars) {

		setFilterKeywords(searchChars);
//		FileDetailDB dbOperations = new FileDetailDB();

		List<String> dataToWrite = new ArrayList<String>();
		int numberOfLinesInFile = 0;
		Integer startLineNumber = dbOperations.getLineNumberFromDB(sourceFile.toString());
		ApplicationLogger.debug(APPLICATION_LOGGER, "Start line number for " + sourceFile.toString() + "is " + startLineNumber);
		try (Stream<String> stream = Files.lines(sourceFile).skip(startLineNumber)) {

			/*
			 * ArrayList<String> fileData = (ArrayList<String>) stream.filter(line ->
			 * line.contains("")) .collect(Collectors.toList());
			 */
			dataToWrite.addAll((ArrayList<String>) stream.filter(this::streamFilter).collect(Collectors.toList()));
		} catch (Exception e) {
			ApplicationLogger.fatal(ERROR_LOGGER, "error in reading file " + sourceFile, e);
		}

		/*
		 * String line; try (Stream<String> lines =
		 * Files.lines(sourceFile).skip(startLineNumber)) { line =
		 * lines.skip(startLineNumber).findFirst().get(); }
		 */
		numberOfLinesInFile = (int) countLine(sourceFile);
		dbOperations.updateLineNumber(sourceFile.toString(), numberOfLinesInFile);
		ApplicationLogger.debug(APPLICATION_LOGGER, "Updating Line Number for " + sourceFile.toString() + "to " + numberOfLinesInFile);
		return dataToWrite;
	}

	private void setFilterKeywords(String searchChars) {
		String[] values = searchChars.split("\\|");
		filterKeywords = new HashSet<String>(Arrays.asList(values));
	}

	private boolean streamFilter(String str) {
//		boolean value = false;
		return filterKeywords.parallelStream().filter(str::contains).findAny().orElse(null) == null ? false : true;
//		return value;
	}

	public static long countLine(Path fileName) {
		long lineCount = 0;
		try (Stream<String> stream = Files.lines(fileName, StandardCharsets.UTF_8)) {
			lineCount = stream.count();
		} catch (IOException e) {
			ApplicationLogger.fatal(ERROR_LOGGER, "error in counting lines " + fileName, e);
		}
		return lineCount;
	}
}
