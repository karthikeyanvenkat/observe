package com.oracle.tools.observe.tests;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringSplitTest {
	public static void main(String...args)
	{
		String s = "test1|test2|test3";
		String[] values = s.split("\\|");
		Set<String> filterKeywords = new HashSet<String>(Arrays.asList(values));
		filterKeywords.forEach(System.out::println);
	}
}
