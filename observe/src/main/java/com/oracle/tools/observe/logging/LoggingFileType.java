package com.oracle.tools.observe.logging;

public class LoggingFileType {
	public static final String PERFORMANCE_LOGGER = "PerformanceLogger";
	public static final String APPLICATION_LOGGER = "ApplicationLogger";
	public static final String ERROR_LOGGER = "ErrorLogger";
	public static final String EVENT_LOGGER = "EventLogger";
}
