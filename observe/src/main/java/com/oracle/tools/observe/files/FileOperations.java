package com.oracle.tools.observe.files;

import static com.oracle.tools.observe.logging.LoggingFileType.ERROR_LOGGER;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oracle.tools.observe.logging.ApplicationLogger;

@Component
public class FileOperations {

	@Autowired
	ApplicationFileReader fileReader;

	List<String> dataToWrite;

	public void processFile(Path sourceFullPath, String destinationFullPath, String csvSaerchValues) {
		fetchDatatoWrite(sourceFullPath, csvSaerchValues);
		writeDataToFile(destinationFullPath);
	}

	private void fetchDatatoWrite(Path sourceFile, String searchChars) {
		dataToWrite = fileReader.readFile(sourceFile, searchChars);
	}

	private void writeDataToFile(String drestinationFile) {
		ApplicationFileWriter fileWritter = new ApplicationFileWriter();
		try {
			fileWritter.writeToFile(drestinationFile, dataToWrite);
		} catch (IOException e) {
			ApplicationLogger.fatal(ERROR_LOGGER, "error in writing file " + drestinationFile, e);
		}
	}

}
