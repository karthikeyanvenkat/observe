package com.oracle.tools.observe.validator;

import static com.oracle.tools.observe.logging.LoggingFileType.APPLICATION_LOGGER;
import static com.oracle.tools.observe.logging.LoggingFileType.PERFORMANCE_LOGGER;

import java.io.File;

import org.springframework.stereotype.Component;

import com.oracle.tools.observe.logging.ApplicationLogger;

@Component
public class ConfigExcelValidator {
	public boolean validateFile(String fileFullPath) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## START ##########");
		boolean returnValue = false;
		File file = new File(fileFullPath);
		if (file.exists() && file.isFile() && file.canRead()) {
			ApplicationLogger.trace(APPLICATION_LOGGER, "File " + fileFullPath + " basic validation success");
			returnValue = true;
		} else {
			ApplicationLogger.trace(APPLICATION_LOGGER, "file.exists: " + file.exists() + " file.isFile: "
					+ file.isFile() + " file.canRead: " + file.canRead());
			returnValue = false;
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "########## END ##########");
		return returnValue;
	}

}
