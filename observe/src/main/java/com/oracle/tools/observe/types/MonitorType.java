package com.oracle.tools.observe.types;

public enum MonitorType {
	FILE,
	DIRECTORY,
	PATTERN
}
