package com.oracle.tools.observe.logging;

public class LogsHelper {
	public static String getMethodName() {
		StackTraceElement[] stk = Thread.currentThread().getStackTrace();
		StackTraceElement element = stk[2];
		String methodName = element.getMethodName() + "(LineNumber-" + element.getLineNumber() + ")";
		return methodName;
	}

	public static String getMessage(String message) {
		String logMessage = getSuperMethodName() + " LogMessage: " + message;
		return logMessage;
	}

	public static String getSuperMethodName() {
		StackTraceElement[] stk = Thread.currentThread().getStackTrace();
		StackTraceElement element = stk[4];
		String methodName = element.getClassName() + "->" + element.getMethodName() + "(LineNumber-" + element.getLineNumber() + ")";
		return methodName;
	}
}
