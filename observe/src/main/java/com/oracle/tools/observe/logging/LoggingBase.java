package com.oracle.tools.observe.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class LoggingBase {
	private static LoggingBase instance;

	private LoggingBase() {

	}

	public static LoggingBase getInstance() {
		if (instance == null) {
			instance = new LoggingBase();
		}
		return instance;
	}

	public void performance(String category, String message) {
		this.getLogger(category).info(message);
	}

	public void info(String category, String message) {
		this.getLogger(category).info(message);
	}

	public void debug(String category, String message) {
		this.getLogger(category).debug(message);
	}

	public void warn(String category, String message) {
		this.getLogger(category).warn(message);
	}

	public void trace(String category, String message) {
		this.getLogger(category).trace(message);
	}

	public void error(String category, String message) {
		this.getLogger(category).error(message);
	}

	public void fatal(String category, String message) {
		this.getLogger(category).fatal(message);
	}

	public void fatal(String category, String message, Throwable exception) {
		this.getLogger(category).fatal(message, exception);
	}

	private Logger getLogger(String category) {
		Logger logger = LogManager.getLogger(category);
		return logger;
	}
}
