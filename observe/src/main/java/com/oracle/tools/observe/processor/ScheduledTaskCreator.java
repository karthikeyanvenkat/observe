package com.oracle.tools.observe.processor;

import static com.oracle.tools.observe.logging.LoggingFileType.APPLICATION_LOGGER;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.oracle.tools.observe.config.data.Data;
import com.oracle.tools.observe.config.data.DataBean;
import com.oracle.tools.observe.logging.ApplicationLogger;
import com.oracle.tools.observe.task.ObserverTask;
import com.oracle.tools.observe.types.MonitorType;

@Component
public class ScheduledTaskCreator {

	@Autowired
	Data data;
	@Autowired
	private ApplicationContext applicationContext;

	public void startObservation() {
		ApplicationLogger.trace(APPLICATION_LOGGER, "Entered Observation");
		List<DataBean> excelData = data.getData();
		if (excelData.size() > 10) {
			ApplicationLogger.warn(APPLICATION_LOGGER,
					"Warning more than 10 configurations for application will impact the performence");

		}

//		ExecutorService threadsExecutor = Executors.newFixedThreadPool(excelData.size());

		excelData.forEach(configuration -> {
			if (configuration.isEnabled()) {
				String pathPattern = configuration.getPathPattern();
				File f = new File(pathPattern);
				MonitorType monitorType = getMonitorype(configuration);
				ObserverTask task = new ObserverTask(configuration);
				task.setMonitorType(monitorType);
				if(monitorType == MonitorType.FILE)
					task.setFileContainerDir(f.getParent());
				applicationContext.getAutowireCapableBeanFactory().autowireBean(task);
//				threadsExecutor.execute(task);
				ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
				scheduler.scheduleAtFixedRate(task, 0, configuration.getMonitorInterval(), TimeUnit.SECONDS);
			}
		});
	}

	private MonitorType getMonitorype(DataBean configuration) {
		MonitorType monitorType;
		String pathPattern = configuration.getPathPattern();
		File f = new File(pathPattern);
		if (f.isDirectory())
			monitorType = MonitorType.DIRECTORY;
		else if (!pathPattern.contains("*")) {
			monitorType = MonitorType.FILE;
		} else
			monitorType = MonitorType.PATTERN;
		return monitorType;
	}

	public void initilize() {

	}
}
