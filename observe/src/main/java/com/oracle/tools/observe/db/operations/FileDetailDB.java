package com.oracle.tools.observe.db.operations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oracle.tools.observe.db.entity.FileDetail;
import com.oracle.tools.observe.db.repository.FileDetailRepository;

@Component
public class FileDetailDB {

	@Autowired
	private FileDetailRepository fileDetailRepository;

	public void updateLineNumber(String filePath, int lastLineNumber) {
		fileDetailRepository.save(new FileDetail(filePath, lastLineNumber));
	}

	public Integer getLineNumberFromDB(String filePath) {
		Integer startLineNumber = 1;
		Optional<FileDetail> tableRowData = fileDetailRepository.findById(filePath);
		if (tableRowData.isPresent())
			startLineNumber = tableRowData.get().getLineNumber();
		return startLineNumber;
	}

	public void reset(String filePath) throws Exception {
		fileDetailRepository.save(new FileDetail(filePath, 1));
	}

	public void delete(String filePath) {
		fileDetailRepository.deleteById(filePath);
	}
}
