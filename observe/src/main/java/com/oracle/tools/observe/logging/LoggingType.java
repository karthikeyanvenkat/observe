package com.oracle.tools.observe.logging;

public enum LoggingType {
	PERFORMANCE_LOGGER("PerformanceLogger"),
	APPLICATION_LOGGER("ApplicationLogger"),
	ERROR_LOGGER("ErrorLogger"),
	EVENT_LOGGER("EventLogger");
	
	public final String fileType;

    private LoggingType(String fileType) {
        this.fileType = fileType;
    }
}
