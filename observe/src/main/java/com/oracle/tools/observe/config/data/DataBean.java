package com.oracle.tools.observe.config.data;

import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class DataBean {
	@CsvBindByName(column = "ConfigID")
	private String configID;
	@CsvBindByName(column = "MonitorFile")
	private String pathPattern;
	@CsvBindByName(column = "DestinationFile")
	private String destinationFile;
	@CsvBindByName(column = "MonitorValues")
	private String monitorValues;
	@CsvBindByName(column = "MonitorInterval")
	private int monitorInterval;
	@CsvBindByName(column = "Enabled")
	private boolean isEnabled;

	@Override
	public String toString() {
		String s = "\nConfigID: " + configID + "\nMonitorFile: " + pathPattern + "\nDestinationFile: " + destinationFile
				+ "\nMonitorValues: " + monitorValues + "\nMonitorInterval: " + monitorInterval + "\nEnabled: "
				+ isEnabled;
		return s;
	}
}
