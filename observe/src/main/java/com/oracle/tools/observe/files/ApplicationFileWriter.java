package com.oracle.tools.observe.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ApplicationFileWriter {

	public void writeToFile(String drestinationFile, List<String> dataToWrite) throws IOException {
		FileWriter writer = null;
		File newFile = new File(drestinationFile);
		/*
		 * if (!newFile.exists()) { writer = new FileWriter(drestinationFile); } else {
		 */
		writer = new FileWriter(drestinationFile, newFile.exists());
//		}
		for (String str : dataToWrite) {
			writer.write(str + System.lineSeparator());
		}
		writer.close();
	}

}
