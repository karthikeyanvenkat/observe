package com.oracle.tools.observe.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterTest {
	private Set<String> filterKeywords = new HashSet<String>(Arrays.asList("PARAG", "CHIKOO", "GUNDU"));

	public static void main(String args[]) throws IOException {
		FilterTest test = new FilterTest();
		test.process();
	}

	private void process() throws IOException {
		List<String> dataToWrite = new ArrayList<String>();
		String filePath = "D:\\JCAP\\WatchDir\\ABC.txt";
		File f = new File(filePath);
		Path file = f.toPath();
		try (Stream<String> stream = Files.lines(file).skip(0)) {

			/*
			 * ArrayList<String> fileData = (ArrayList<String>) stream.filter(line ->
			 * line.contains("")) .collect(Collectors.toList());
			 */

			dataToWrite
					.addAll((ArrayList<String>) stream.filter(this::streamFilter).collect(Collectors.toList()));
			dataToWrite.forEach(System.out::println);
		}
	}
	
	private boolean streamFilter(String str) {
		boolean value =  filterKeywords.parallelStream().filter(str::contains).findAny().orElse(null) == null ? false : true;
		return value;
	}
}
