package com.oracle.tools.observe.config.loader;

import java.io.FileReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvToBeanBuilder;
import com.oracle.tools.observe.config.data.Data;
import com.oracle.tools.observe.config.data.DataBean;
import com.oracle.tools.observe.logging.ApplicationLogger;
import com.oracle.tools.observe.logging.LoggingFileType;

@Component
public class CsvDataLoader implements IDataLoader {
	@Autowired
	private Data data;

	@Override
	public boolean loadData(String fileFullPath) {
		boolean returnValue = false;
		ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, "Loading Data from CSV");
		try {
			List<DataBean> fileData = new CsvToBeanBuilder<DataBean>(new FileReader(fileFullPath))
					.withType(DataBean.class).build().parse();
			fileData.forEach(dataBean -> {ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, dataBean.toString());});
			data.setData(fileData);
			returnValue = true;
			ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, "Data Loaded successfully");
		} catch (Exception e) {
			ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER, "Invalid File Format", e);
		}
		return returnValue;
	}
}
