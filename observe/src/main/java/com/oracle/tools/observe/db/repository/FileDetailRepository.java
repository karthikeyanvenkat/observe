package com.oracle.tools.observe.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oracle.tools.observe.db.entity.FileDetail;

public interface FileDetailRepository extends JpaRepository<FileDetail, String>{
	
}
