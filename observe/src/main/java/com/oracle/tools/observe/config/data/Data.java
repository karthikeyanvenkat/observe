package com.oracle.tools.observe.config.data;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Data implements IData {

	List<DataBean> fileData;
	
	@Override
	public List<DataBean> getData() {
		return fileData;
	}

	@Override
	public void setData(List<DataBean> fileData) {
		this.fileData = fileData;
		
	}

	/*
	 * List<DataPOJO> fileData;
	 * 
	 * public Data() { fileData = new ArrayList<DataPOJO>(); }
	 * 
	 * @Override public List<DataPOJO> getData() { return fileData; }
	 * 
	 * @Override public void setData(DataPOJO rowData) { fileData.add(rowData); }
	 */
	
	

}
