package com.oracle.tools.observe.logging;

public class ApplicationLogger{
	public static ApplicationLogger aplicationLogger = new ApplicationLogger();
	private ApplicationLogger()
	{
	}
	public static void performance(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().performance(category, message);
	}
	
	public static void info(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().info(category, message);
	}
	public static void debug(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().debug(category, message);
	}
	
	public static void warn(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().warn(category, message);
	}
	public static void trace(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().trace(category, message);
	}
	public static void error(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().error(category, message);
	}
	public static void fatal(String category, String message) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().fatal(category, message);
	}
	public static void fatal(String category, String message, Throwable exception) {
		message = LogsHelper.getMessage(message);
		LoggingBase.getInstance().fatal(category, message, exception);
	}
}
