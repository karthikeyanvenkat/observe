package com.oracle.tools.observe.task;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import com.oracle.tools.observe.config.data.DataBean;
import com.oracle.tools.observe.db.operations.FileDetailDB;
import com.oracle.tools.observe.files.FileOperations;
import com.oracle.tools.observe.logging.ApplicationLogger;
import com.oracle.tools.observe.logging.LoggingFileType;
import com.oracle.tools.observe.types.MonitorType;

import lombok.Setter;

public class ObserverTask implements Runnable {

	@Autowired
	FileDetailDB dbOperation;
	@Autowired
	private FileOperations fileOperations;
	@Setter
	private MonitorType monitorType;
	@Setter
	private String fileContainerDir;

	private DataBean data;

	public ObserverTask(DataBean data) {
		this.data = data;
	}

	@Override
	public void run() {
		switch (monitorType) {
		case DIRECTORY:
			monitorDirectory(data);
			break;
		case FILE:
			monitorFile(data, fileContainerDir);
			break;
		case PATTERN:
			monitorPattern(data);
		default:
			break;
		}
	}

	private void monitorDirectory(DataBean configData) {
		try {
			WatchService watchService = FileSystems.getDefault().newWatchService();
			Path dir = Paths.get(configData.getPathPattern());
			dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey watchKey = watchService.take();
			if (watchKey != null) {
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					Path sourceFullPath = dir.resolve(event.context().toString());
					ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
							"Triggered " + event.kind() + " Change detected in: " + sourceFullPath);
					Kind<?> kind = event.kind();
					if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
//						FileDetailDB dbOperation = new FileDetailDB();
						try {
							dbOperation.reset(sourceFullPath.toString());
						} catch (Exception e) {
							ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
									"dbOperation.reset failed for " + sourceFullPath, e);
						}
						ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, "File Created: " + sourceFullPath);
					} else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
						// FileOperations fileOperations = new FileOperations();
						fileOperations.processFile(sourceFullPath, configData.getDestinationFile(),
								configData.getMonitorValues());
						ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, "File Modified: " + sourceFullPath);
					} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
//						FileDetailDB dbOperation = new FileDetailDB();
						dbOperation.delete(sourceFullPath.toString());
						ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER, "File Deleted: " + sourceFullPath);
					}
				}
				watchKey.reset();
			}
		} catch (Exception e) {
			ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
					"Failed Monitoring Directory : " + configData.getPathPattern(), e);
		} 
	}

	private void monitorPattern(DataBean configData) {
		String matching = configData.getPathPattern();
		String directory = matching.substring(0, matching.lastIndexOf(File.separator));
		matching = matching.substring(matching.lastIndexOf(File.separator) + 1);
		matching = matching.replaceAll("\\.", "\\\\.");
		matching = matching.replaceAll("\\*", ".*");
		try {
			WatchService watchService = FileSystems.getDefault().newWatchService();
			Path dir = Paths.get(directory);
			dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey watchKey = watchService.take();
			if (watchKey != null) {
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					Path sourceFullPath = dir.resolve(event.context().toString());
					Pattern pattern = Pattern.compile(matching);
					Matcher matcher = pattern.matcher(event.context().toString());
					if (matcher.find() && directory.equals(dir.toString())) {
						Kind<?> kind = event.kind();
						if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
							try {
								dbOperation.reset(sourceFullPath.toString());
							} catch (Exception e) {
								ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
										"dbOperation.reset failed for " + sourceFullPath, e);
							}
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Created: " + sourceFullPath);
						} else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
							// FileOperations fileOperations = new FileOperations();
							fileOperations.processFile(sourceFullPath, configData.getDestinationFile(),
									configData.getMonitorValues());
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Modified: " + sourceFullPath);
						} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
//						FileDetailDB dbOperation = new FileDetailDB();
							dbOperation.delete(sourceFullPath.toString());
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Deleted: " + sourceFullPath);
						}
					}
					watchKey.reset();
				}
			}
		} catch (Exception e) {
			ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
					"Failed Monitoring Pattern : " + configData.getPathPattern(), e);
		}
	}

	private void monitorFile(DataBean configData, String dirPath) {
		try {
			WatchService watchService = FileSystems.getDefault().newWatchService();
			Path dir = Paths.get(dirPath);
			dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey watchKey = watchService.take();
			if (watchKey != null) {
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					Path sourceFullPath = dir.resolve(event.context().toString());
					if (sourceFullPath.toString().equals(configData.getPathPattern())) {
						Kind<?> kind = event.kind();
						if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
//						FileDetailDB dbOperation = new FileDetailDB();
							try {
								dbOperation.reset(sourceFullPath.toString());
								fileOperations.processFile(sourceFullPath, configData.getDestinationFile(),
										configData.getMonitorValues());
							} catch (Exception e) {
								ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
										"dbOperation.reset failed for " + sourceFullPath, e);
							}
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Created: " + sourceFullPath);
						} else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
							// FileOperations fileOperations = new FileOperations();
							fileOperations.processFile(sourceFullPath, configData.getDestinationFile(),
									configData.getMonitorValues());
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Modified: " + sourceFullPath);
						} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
//						FileDetailDB dbOperation = new FileDetailDB();
							dbOperation.delete(sourceFullPath.toString());
							ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
									"File Deleted: " + sourceFullPath);
						}
					}
					watchKey.reset();
				}
			}
		} catch (Exception e) {
			ApplicationLogger.fatal(LoggingFileType.ERROR_LOGGER,
					"Failed Monitoring File : " + configData.getPathPattern(), e);
		}
	}
}
